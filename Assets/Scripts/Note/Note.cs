// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;
using System;

public class Note : MonoBehaviour
{
	public NoteManager noteManager;
	public GameObject noteHint;

	public void OnTriggerEnter(Collider other)
	{
		noteHint.SetActive(true);
	}

	public void OnTriggerExit(Collider other)
	{
		noteHint.SetActive(false);
	}

	public void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player") {
			if (Input.GetKeyDown(KeyCode.E)) {
				noteManager.readNote();
				gameObject.SetActive(false);
			}
		}
	}
}