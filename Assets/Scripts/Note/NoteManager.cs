// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class NoteManager : MonoBehaviour
{
	public List<GameObject> events;

	[TextArea(3, 5)]
	public List<string> noteContents;

	public int collectedNotes = 0;

	public Text noteText;
	public Text noteNumberText;
	public GameObject notePanel;
	public LockCursor lockCursor;

	public bool hasNoteBeenRead(int noteNumber)
	{
		if (collectedNotes + 1 > noteNumber) {
			return true;
		}
		else {
			return false;
		}
	}

	public void readNote()
	{
		if (collectedNotes < noteContents.Count) {
			noteText.text = noteContents[collectedNotes];
			collectedNotes++;
			notePanel.SetActive(true);
			noteNumberText.text = "Note " + collectedNotes;
			lockCursor.unlockCursor();
			updateEvents(collectedNotes);
		}
	}

	public void updateEvents(int noteNumber)
	{
		for (int index = 0; index < events.Count; index++) {
			events[index].SendMessage("onNoteRead", noteNumber, SendMessageOptions.DontRequireReceiver);
		}
	}
}