// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
[Serializable]
public struct NotesRead
{
	public bool hasBeenRead;
	public int noteNumber;
}