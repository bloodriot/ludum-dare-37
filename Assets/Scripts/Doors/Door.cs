// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour
{
	public bool isLocked = true;
	public DisplayMessage displayMessage;
	public GameObject interactionHint;

	public void interact()
	{
		if (isLocked) {
			displayMessage.showMessage("This door is locked.");
		}
		else {
			SceneManager.LoadScene("Ending");
		}
	}
	
	public void OnTriggerEnter(Collider other)
	{
		if (interactionHint != null) {
			interactionHint.SetActive(true);
		}
	}
	
	public void OnTriggerExit(Collider other)
	{
		if (interactionHint != null) {
			interactionHint.SetActive(false);
		}
	}
	
	public void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player") {
			if (Input.GetKeyDown(KeyCode.E)) {
				interact();
			}
		}
	}
}