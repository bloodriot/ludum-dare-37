// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

public class FirstLight : MonoBehaviour
{
	public NoteManager noteManager;
	public GameObject whiteLight;
	public AudioClip eventStinger;
	public bool eventPlayed = false;

	public void onNoteRead(int noteNumber)
	{
		if (!eventPlayed) {
			// This event requires notes 1 and 2 to be read before it will trigger
			if (noteManager.hasNoteBeenRead(2)) {
				triggerEvent();
			}
		}
	}

	public void triggerEvent()
	{
		eventPlayed = true;
		whiteLight.SetActive(true);
		AudioSource.PlayClipAtPoint(eventStinger, transform.position, 1f);
	}
}