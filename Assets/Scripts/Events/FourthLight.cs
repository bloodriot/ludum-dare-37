// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

public class FourthLight : MonoBehaviour
{
	public NoteManager noteManager;
	public GameObject roomLight;
	public AudioClip eventStinger;
	public bool eventPlayed = false;

	public void onNoteRead(int noteNumber)
	{
		if (!eventPlayed) {
			// This event requires notes 13 through 18 to be read before it will trigger
			if (noteManager.hasNoteBeenRead(18)) {
				triggerEvent();
			}
		}
	}
	
	public void triggerEvent()
	{
		eventPlayed = true;
		roomLight.SetActive(true);
		AudioSource.PlayClipAtPoint(eventStinger, transform.position, 1f);
	}
}