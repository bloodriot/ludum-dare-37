// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;

public class EnterLight : MonoBehaviour
{
	public List<GameObject> currentWorld;
	public List<GameObject> newWorld;
	public AudioClip soundSFX;
	[TextArea(3, 5)]
	public string messageToDisplay;
	public DisplayMessage displayMessage;
	public bool used = false;

	public void OnTriggerEnter(Collider other)
	{
		if (!used) {
			if (other.tag == "Player") {
				used = true;
				AudioSource.PlayClipAtPoint(soundSFX, transform.position, 0.75f);
				for (int index = 0; index < newWorld.Count; index++) {
					newWorld[index].SetActive(true);
				}
				displayMessage.showMessage(messageToDisplay);
				for (int index = 0; index < currentWorld.Count; index++) {
					currentWorld[index].SetActive(false);
				}
			}
		}
	}
}