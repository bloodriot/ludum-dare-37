// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

public class ThirdLight : MonoBehaviour
{
	public NoteManager noteManager;
	public GameObject roomLight;
	public AudioClip eventStinger;
	public bool eventPlayed = false;

	public void onNoteRead(int noteNumber)
	{
		if (!eventPlayed) {
			// This event requires notes 8 through 12 to be read before it will trigger
			if (noteManager.hasNoteBeenRead(12)) {
				triggerEvent();
			}
		}
	}
	
	public void triggerEvent()
	{
		eventPlayed = true;
		roomLight.SetActive(true);
		AudioSource.PlayClipAtPoint(eventStinger, transform.position, 1f);
	}
}