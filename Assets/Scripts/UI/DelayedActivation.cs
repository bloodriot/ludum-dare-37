// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedActivation : MonoBehaviour
{
	public List<GameObject> objectsToActivate;
	public float delay;

	public void OnEnable()
	{
		StartCoroutine(activate());
	}

	public IEnumerator activate()
	{
		yield return new WaitForSeconds(delay);
		for (int index = 0; index < objectsToActivate.Count; index++) {
			objectsToActivate[index].SetActive(true);
		}
	}
}