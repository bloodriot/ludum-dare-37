// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using System;
using UnityEngine;
using UnityEngine.UI;

public class DisplayMessage : MonoBehaviour
{
	public GameObject messagePanel;
	public Text messageText;
	public LockCursor lockCursor;

	public void showMessage(string message)
	{
		messageText.text = message;
		messagePanel.SetActive(true);
		lockCursor.unlockCursor();
	}
}

