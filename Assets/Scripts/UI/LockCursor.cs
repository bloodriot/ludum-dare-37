// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class LockCursor : MonoBehaviour
{
	public FirstPersonController controller;

	public void lockCursor()
	{
		controller.enabled = true;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	public void unlockCursor()
	{
		controller.enabled = false;
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}
}