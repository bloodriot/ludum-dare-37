// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

public class Key : MonoBehaviour
{
	public Door door;
	public DisplayMessage displayMessage;
	public GameObject hint;

	public void OnTriggerEnter(Collider other)
	{
		hint.SetActive(true);
	}
	
	public void OnTriggerExit(Collider other)
	{
		hint.SetActive(false);
	}
	
	public void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player") {
			if (Input.GetKeyDown(KeyCode.E)) {
				door.isLocked = false;
				gameObject.SetActive(false);
				displayMessage.showMessage("You picked up a key.");
			}
		}
	}
}